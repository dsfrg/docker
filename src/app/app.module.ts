import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecepiesComponent } from './recepies/recepies.component';
import { ResepieListComponent } from './recepies/resepie-list/resepie-list.component';
import { ResepieDetailComponent } from './recepies/resepie-detail/resepie-detail.component';
import { ResepieItemComponent } from './recepies/resepie-list/resepie-item/resepie-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { HeaderComponent } from './header/header/header.component';
import { DropdownDirective } from './Shared/dropdown.directive';
import { ShoppingListService } from './shopping-list/shopping-list.service';

@NgModule({
   declarations: [
      AppComponent,
      RecepiesComponent,
      ResepieListComponent,
      ResepieDetailComponent,
      ResepieItemComponent,
      ShoppingListComponent,
      ShoppingEditComponent,
      HeaderComponent,
      DropdownDirective
   ],
   imports: [
      BrowserModule,
      FormsModule,
      AppRoutingModule
   ],
   providers: [ShoppingListService],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
