import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  OnDestroy
} from '@angular/core';

import { Ingredient } from '../../ingredient.model';
import { ShoppingListService } from '../shopping-list.service';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
 /* @ViewChild('nameInput', { static: false }) nameInputRef: ElementRef;
  @ViewChild('amountInput', { static: false }) amountInputRef: ElementRef;*/
  // form: FormGroup;
  @ViewChild('f', { static: false }) listForm: NgForm;
  editSubscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;
  constructor(private slService: ShoppingListService) { }

  ngOnInit() {
    /*this.form = new FormGroup({
        name: new FormControl(null),
        amount: new FormControl(null)
      }
    );*/
      this.editSubscription = this.slService.ingredientsToEdit.subscribe(
        (index: number) => {
          this.editMode = true;
          this.editedItemIndex = index;
          this.editedItem = this.slService.getIngredients()[this.editedItemIndex];
          this.listForm.setValue({
            name: this.editedItem.name,
            amount: this.editedItem.amount
          });
        }
      );
  }

  ngOnDestroy() {
    this.editSubscription.unsubscribe();
  }

  onAddItem( form: NgForm) {
    /*const ingName = this.nameInputRef.nativeElement.value;
    const ingAmount = this.amountInputRef.nativeElement.value;*/
    const value = form.value;
    const newIngredient = new Ingredient(value.name, value.amount);
    if (this.editMode) {
      this.slService.updateIngredients(this.editedItemIndex, newIngredient);
    } else {
      this.slService.addIngredient(newIngredient);
    }
    this.listForm.reset();
    this.editMode = false;
  }

  onResetForm() {
    this.listForm.reset();
    this.editMode = false;
  }

  onDeleteItem() {
    this.slService.deleteIngredients(this.editedItemIndex);
    this.onResetForm();
  }
}
