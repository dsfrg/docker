import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

  ingredients: Ingredient[];
  igSubscription: Subscription;
  constructor(private listService: ShoppingListService) { }

  ngOnInit(): void {
    this.ingredients = this.listService.getIngredients();
    this.igSubscription = this.listService.ingrediansChanged.subscribe(
      (ing: Ingredient[]) => {
        this.ingredients = ing;
      }
    );
  }

  ngOnDestroy() {
    this.igSubscription.unsubscribe();
  }

  add(item: Ingredient) {
    this.ingredients.push(item);
  }

  onEditItem(id: number) {
    this.listService.ingredientsToEdit.next(id);
  }

}
