import { Injectable, EventEmitter } from '@angular/core';
import { Ingredient } from '../ingredient.model';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  ingrediansChanged = new Subject<Ingredient[]>();
  ingredientsToEdit = new Subject<number>();
  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10)
  ];
  constructor() { }

  getIngredients() {
    return this.ingredients.slice();
  }

  addIngredient(ingr: Ingredient) {
    this.ingredients.push(ingr);
    this.ingrediansChanged.next(this.ingredients.slice());
  }

  addIngrediens(ingr: Ingredient[]) {
      this.ingredients.push(...ingr);
      this.ingrediansChanged.next(this.ingredients.slice());
  }

  updateIngredients(index: number, newIngredients: Ingredient) {
    this.ingredients[index] = newIngredients;
    this.ingrediansChanged.next(this.ingredients.slice());
  }

  deleteIngredients(index: number) {
    this.ingredients.splice(index, 1);
    this.ingrediansChanged.next(this.ingredients.slice());
  }
}
