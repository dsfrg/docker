import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe('A Test Recipe', 'This is simply a test', 
    'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg', 
    [
      new Ingredient('meat', 1),
      new Ingredient('something', 4)
    ]),
    new Recipe('Another Test Recipe',
     'This is simply a test', 
     'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
     [
      new Ingredient('meat', 1),
      new Ingredient('something', 4)
     ])
  ];
constructor(private slService: ShoppingListService) { }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number): Recipe {
    return this.recipes.slice()[id];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngrediens(ingredients);
  }

}
