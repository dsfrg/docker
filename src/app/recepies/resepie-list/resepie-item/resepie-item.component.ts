import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../../recipe.model';
import { RecipeService } from '../../Recipe.service';

@Component({
  selector: 'app-resepie-item',
  templateUrl: './resepie-item.component.html',
  styleUrls: ['./resepie-item.component.css']
})
export class ResepieItemComponent implements OnInit {
  @Input() recepie: Recipe;
  @Input() id: number;
  ngOnInit(): void {
  }

}
