import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResepieItemComponent } from './resepie-item.component';

describe('ResepieItemComponent', () => {
  let component: ResepieItemComponent;
  let fixture: ComponentFixture<ResepieItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResepieItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResepieItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
