import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../Recipe.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resepie-list',
  templateUrl: './resepie-list.component.html',
  styleUrls: ['./resepie-list.component.css']
})
export class ResepieListComponent implements OnInit {
  recipes: Recipe[];
  constructor(private resipeService: RecipeService, private router: Router, private rout: ActivatedRoute) { }

  ngOnInit() {
    this.recipes = this.resipeService.getRecipes();
  }

  onNewRecipe() {
    this.router.navigate(['new'], {relativeTo: this.rout});
  }
}
