import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResepieListComponent } from './resepie-list.component';

describe('ResepieListComponent', () => {
  let component: ResepieListComponent;
  let fixture: ComponentFixture<ResepieListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResepieListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResepieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
