import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../Recipe.service';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';

@Component({
  selector: 'app-resepie-detail',
  templateUrl: './resepie-detail.component.html',
  styleUrls: ['./resepie-detail.component.css']
})
export class ResepieDetailComponent implements OnInit {
  recipe: Recipe;
  id: number;
  constructor(private recipeService: RecipeService, private acvtivatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
      this.acvtivatedRoute.params
      .subscribe(
        (params: Params) => {
          this.id = +params[`id`];
          this.recipe  = this.recipeService.getRecipe(this.id);
        }
      );
  }

  onAddToShoppingList() {
    this.recipeService.addIngredientsToShoppingList(this.recipe.ingrediants);
  }

  onRetirectToEdit() {
    this.router.navigate(['edit'], {relativeTo: this.acvtivatedRoute})
  }
}
