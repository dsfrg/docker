import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResepieDetailComponent } from './resepie-detail.component';

describe('ResepieDetailComponent', () => {
  let component: ResepieDetailComponent;
  let fixture: ComponentFixture<ResepieDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResepieDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResepieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
